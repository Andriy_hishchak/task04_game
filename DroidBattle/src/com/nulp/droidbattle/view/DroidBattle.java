package com.nulp.droidbattle.view;

import com.nulp.droidbattle.controller.BattleArena;
import com.nulp.droidbattle.model.Droid;

public class DroidBattle {

  public void startGame() {
    Droid droid1 = new Droid("c3p0", 100, 10);
    Droid droid2 = new Droid("r2d2", 100, 10);

    BattleArena battleArena = new BattleArena(droid1, droid2);
    battleArena.fight();
  }
}
